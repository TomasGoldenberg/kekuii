import 'firebase/auth';
import 'firebase/database';
import PropTypes from 'prop-types';
import firebase from 'firebase/app';
import { useEffect } from 'react';
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
// redux
import { store } from '../../redux/store';
// hooks
import useAuth from '../../hooks/useAuth';
//
import { firebaseConfig } from '../../config';

// ----------------------------------------------------------------------

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const rrfProps = {
  firebase,
  config: {
    userProfile: 'users'
  },
  dispatch: store.dispatch
};

const ADMIN_EMAILS = ['demo@minimals.cc'];

// ----------------------------------------------------------------------

FirebaseProvider.propTypes = {
  children: PropTypes.node
};

export default function FirebaseProvider({ children }) {
  const { isAuthenticated } = useAuth();

  useEffect(() => {
    const getInitialize = async () => {
      try {
        firebase.auth().onAuthStateChanged((user) => {
          if (user && isAuthenticated) {
            firebase
              .database()
              .ref('users')
              .set(
                {
                  role: ADMIN_EMAILS.includes(user.email) ? 'admin' : 'user'
                },
                { merge: true }
              );
          }
        });
      } catch (error) {
        console.error(error);
      }
    };

    getInitialize();
  }, [isAuthenticated]);

  return (
    <ReactReduxFirebaseProvider {...rrfProps}>
      {children}
    </ReactReduxFirebaseProvider>
  );
}
